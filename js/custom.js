function categoriesList() {
    $('.search-menu-wrapper').on("click", function() {
		$('.search-dropdown').removeClass('dropdown-hidden').addClass('dropdown-visible');
    });
}

function newsSlideShow() {
    var divs= $('.blog-news .info-block-slideshow'),
        now = divs.filter(':visible'),
        next = now.next().length ? now.next() : divs.first(),
        speed = 5000;

    now.fadeOut(speed);
    next.fadeIn(speed);
}

function shopBy() {
	$('.filter-list').hide('slow');
	$(this).next('.filter-list').show('slow');
}

function sortOption() {
	var sortOption = $(this).text();
	var parent = $(this).parent();
	$(parent).siblings('.filter-btn').text(sortOption);
}

function listSort() {
	$('.view-as-filter').removeClass('view-as-active');
	$(this).addClass('view-as-active');
	$('.product-product-page').removeClass('large-4 medium-4').addClass('medium-12 large-12 product-list-view');
}

function blockSort() {
	$('.view-as-filter').removeClass('view-as-active');
	$(this).addClass('view-as-active');
	$('.product-product-page').removeClass('medium-12 large-12 product-list-view').addClass('medium-4 large-4');

}

function alphabeticallySort() {
	var $divs = $('.product-product-page');
	var alphabeticallyOrderedDivs = $divs.sort(function (a, b) {
        return $(a).find(".product-name").text() > $(b).find(".product-name").text();
    });
    $(".products").html(alphabeticallyOrderedDivs);
}

function searchInput() {
	$('.search-dropdown').show('slow');
}

function registrationPopUpShow() {
	var top = $(window).scrollTop();

	$('body').css({
		'margin': '0',
		'height': '100%',
		'overflow': 'hidden'
	});

	$('.login-pop-up').css({
		'display': 'block',
		'top': top + 'px'
	});
}

function registrationPopUpHide() {
	$('.login-pop-up').css({
		'display': 'none'
	});

	$('body').css({
		'margin': '0',
		'height': '100%',
		'overflow': 'auto'
	})
}

function validateEmail() {
	var email = $('.email-input').val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(email)){
    	alert('All good;');
    } else if(!re.test(email)){
    	alert('Wrong email address');
    }
}

function mobileMenu() {
	$(this).next('.menubar-menu').toggle('slow');
}

$(function() {
	//categoriesList();
	$('.slider-wrapper').slick({
		dots: true,
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000
	});
	var slideOne = '<h2 class="dot-header">Pink Shoes</h2><p class="dot-description">Now af $145,99</p>'
	var slideTwo = '<h2 class="dot-header">Anna Field</h2><p class="dot-description">Limited Edition</p>'
	var slideThree = '<h2 class="dot-header">Prada</h2><p class="dot-description">Summer is coming</p>'
	$('.slick-dots li:nth-child(1)').append(slideOne);
	$('.slick-dots li:nth-child(2)').append(slideTwo);
	$('.slick-dots li:nth-child(3)').append(slideThree);

	setInterval(newsSlideShow, 2000);

	$(".filter-btn").on( "click", shopBy );
	$(".list-view").on( "click", listSort );
	$(".block-view").on( "click", blockSort );
	$(".alphabetically-view").on( "click", alphabeticallySort );
	$(".filter-list li").on( "click", sortOption );
	$(".search-input").on( "click", searchInput );

	$(".registration-btn-span").on( "click", registrationPopUpShow);
	$(".close-form").on( "click", registrationPopUpHide);
	$(".submit-btn").on( "click", validateEmail);
	$(".mobile-menu").on( "click", mobileMenu);

	$(".filter-btn").on('click',function( e ){
	     e.stopPropagation();
	});

	$(document).on('click', function( e ) {
		if( e.target.className != 'filter-list' ){
		   $(".filter-list").hide('slow');
		}   
	});

	$(".search-input").on('click',function( e ){
	     e.stopPropagation();
	});

	$(document).on('click', function( e ) {
		if( e.target.className != 'search-dropdown' ){
		   $(".search-dropdown").hide('slow');
		}   
	});

	$( function() {
		$( ".login-form" ).draggable();
	});
});

